import { defineConfig, devices } from "@playwright/test";
export default defineConfig({
  testDir: "./tests",
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: process.env.CI ? 1 : 0,
  workers: process.env.CI ? 2 : undefined,
  reporter: "html",
  use: {
    actionTimeout: 0,
    trace: "on",
    headless: true,
    video: "on",
    screenshot: "on",
  },

  projects: [
    {
      name: "mainChrome",
      testMatch: /.*.spec.ts/,
      use: {
        ...devices["Desktop Chrome"],
      },
    },
  ],
});
