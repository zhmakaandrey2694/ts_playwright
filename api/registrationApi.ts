import { APIRequestContext } from "@playwright/test";
import { expect } from "../pom/castomFixtures";
import { BaseApi } from "./baseApi";

export class RegistrationApi extends BaseApi {
  constructor(request: APIRequestContext) {
    super(request);
  }

  async getCookie(): Promise<string> {
    const loginRes = await this.request.post("a/login/", {
      data: {
        username: "interviewUser",
        password: "Interview",
        persistent: true,
      },
    });
    expect(loginRes).toBeOK();

    return loginRes.headers()["set-cookie"].substring(10, 74);
  }

  async deactivateCourse(loginAuth: string, registrationId: number) {
    const deactivateRes = await this.request.put(
      "a/course_registrations/roster/",
      {
        headers: {
          "X-CSRFToken": loginAuth,
          Referer:
            "https://demo-auto.testing.kmionline.com/a/course_registrations/roster/",
        },
        data: { registrations: [{ id: registrationId }], action: "deactivate" },
      }
    );
    expect(deactivateRes).toBeOK();
  }
}
