import { expect, test } from "../../pom/castomFixtures";
import { RegistrationApi } from "../../api/registrationApi";

test.describe("API tests", () => {
  test("API test", async ({ request }) => {
    const data = {
      userId: 40188852,
      courseId: 1000010997,
      statusName: "Not Started",
    };

    // course registration
    const postRegistrationRes = await request.post("api/registrations/", {
      data: {
        userId: data.userId,
        courseId: data.courseId,
      },
    });
    expect(postRegistrationRes).toBeOK();

    // check the course was registered
    const getRegistrationRes = await request.get("api/registrations/", {
      data: {
        userId: data.userId,
        courseId: data.courseId,
      },
    });
    const getRegistrationBody = JSON.parse(await getRegistrationRes.text())[1];

    expect(getRegistrationBody.userId).toEqual(data.userId);
    expect(getRegistrationBody.courseId).toEqual(data.courseId);
    expect(getRegistrationBody.statusName).toEqual(data.statusName);

    // ****************************** for deactivating course
    // need to add that into fixtures 
    const registrationApi = new RegistrationApi(request);
    const loginAuth = await registrationApi.getCookie();
    await registrationApi.deactivateCourse(loginAuth, getRegistrationBody.id);
  });
});
