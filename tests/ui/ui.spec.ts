import { test } from "../../pom/castomFixtures";

test("E2e Test", async ({ app }) => {
  /* Basic Auth login
   * Initialized inside fixture
   */

  await app.mainPage.goto();
  await app.mainPage.clickCatalogBtn();
  await app.catalogPage.checkRecentlyUpdatedCoursesCount();

  await app.catalogPage.fillSearchField("LMS - eCommerce");
  await app.catalogPage.clickSearchBtn();
  await app.catalogPage.checkFirstCourseTitle(
    "LMS - eCommerce KMI Infographic"
  );
  await app.catalogPage.checkFirstCoursePrice("$150.00");

  await app.catalogPage.openFirstCourse();
  await app.coursePage.checkPrice("$150.00");
});
