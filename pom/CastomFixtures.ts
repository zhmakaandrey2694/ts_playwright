import { test as base } from "@playwright/test";

import { App } from "./app";

type CustomFixtures = {
  app: App;
};

export const test = base.extend<CustomFixtures>({
  app: async ({ context, browser }, use) => {
    const contextWithCreds = await browser.newContext({
      httpCredentials: {
        username: "testing",
        password: "1",
      },
    });
    const pageWithCreds = await contextWithCreds.newPage();
    await use(new App(pageWithCreds, context, browser));
  },
  request: async ({ playwright }, use) => {
    const apiRequestContext = await playwright.request.newContext({
      baseURL: "https://demo-auto.testing.kmionline.com/",
      extraHTTPHeaders: {
        Authorization: "Token 4E853B08-AE36-4BA1-87FC-4BBF6F965734",
      },
    });
    await use(apiRequestContext);
  },
});

export { expect } from "@playwright/test";
