import { Page, expect } from "@playwright/test";
import { BasePage } from "./basePage";

export class CatalogPage extends BasePage {
  constructor(readonly page: Page) {
    super(page, "lms/full-catalog?type=course");
  }
  readonly recentlyUpdatedCourses = this.page.locator(
    '//simple-course-tile[@course = "course"]'
  );
  readonly searchField = this.page.locator('//input[@placeholder="Search"]');
  readonly searchBtn = this.page.locator('button[aria-label="Search"]');
  readonly courses = this.page.locator(".content-list-item");
  readonly firstCourseTitle = this.courses
    .first()
    .locator(".horizontal-course-tile-title");
  readonly firstCoursePrice = this.courses
    .first()
    .locator(".tile-course-price");

  async fillSearchField(text: string) {
    await this.searchField.fill(text);
  }

  async clickSearchBtn() {
    await this.searchBtn.click();
    await this.page.waitForResponse((resp) => resp.url().includes("/search/"));
  }

  async openFirstCourse() {
    await this.firstCourseTitle.locator("a").click();
  }

  // Asserts
  async checkRecentlyUpdatedCoursesCount() {
    await expect(this.recentlyUpdatedCourses.first()).toBeInViewport();
    expect((await this.recentlyUpdatedCourses.all()).length).toEqual(3);
  }

  async checkFirstCourseTitle(text: string) {
    expect(await this.firstCourseTitle.innerText()).toEqual(text);
  }

  async checkFirstCoursePrice(text: string) {
    expect(await this.firstCoursePrice.innerText()).toEqual(text);
  }
}
