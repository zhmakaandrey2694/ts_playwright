import { Page, expect } from "@playwright/test";
import { BasePage } from "./basePage";

export class CoursePage extends BasePage {
  constructor(readonly page: Page) {
    super(page, "");
  }
  readonly price = this.page.locator(".price-title strong");

  // Asserts
  async checkPrice(text: string) {
    expect(await this.price.innerText()).toEqual(text);
  }
}
