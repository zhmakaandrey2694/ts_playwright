import { Page } from "@playwright/test";
import { BasePage } from "./basePage";

export class MainPage extends BasePage {
  constructor(readonly page: Page) {
    super(page, "lms/welcome");
  }
  readonly catalogBtn = this.page
    .locator(".header-group")
    .getByTitle("Open Catalog");

  async clickCatalogBtn() {
    await this.catalogBtn.click();
  }
}
