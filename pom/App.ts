import { Browser, BrowserContext, Page } from "@playwright/test";
import { PageProvider } from "./pageProvider";

export class App extends PageProvider {
  constructor(
    public page: Page,
    public browserContext: BrowserContext,
    public browser: Browser
  ) {
    super(page);
  }
}
