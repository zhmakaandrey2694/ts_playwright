import { Page } from "@playwright/test";
import { MainPage } from "./pages/main";
import { CatalogPage } from "./pages/catalog";
import { CoursePage } from "./pages/course";

export abstract class PageProvider {
  private _coursePage: CoursePage | undefined;
  private _catalogPage: CatalogPage | undefined;
  private _mainPage: MainPage | undefined;

  get catalogPage(): CatalogPage {
    this._catalogPage ??= new CatalogPage(this.page);
    return this._catalogPage;
  }

  get mainPage(): MainPage {
    this._mainPage ??= new MainPage(this.page);
    return this._mainPage;
  }

  get coursePage(): CoursePage {
    this._coursePage ??= new CoursePage(this.page);
    return this._coursePage;
  }

  protected constructor(public page: Page) {}
}
